import {Injectable} from '@angular/core';
import {ConnectionBackend, Headers, RequestOptions, RequestOptionsArgs} from '@angular/http';
import {PreloadHttp} from 'angular2-platform-node/node-http';

import {AbstractCookie} from '../cookie/AbstractCookie';

@Injectable()
export class HttpService extends PreloadHttp {

  constructor(_backend: ConnectionBackend,
              _defaultOptions: RequestOptions,
              private __cookie: AbstractCookie) {
    super(_backend, _defaultOptions);
  }

  public request(url: string, options?: RequestOptionsArgs) {
    return super.request(url, this.getOptions(options));
  }

  public get(url: string, options?: RequestOptionsArgs) {
    return super.get(url, this.getOptions(options));
  }

  public post(url: string, body: string, options?: RequestOptionsArgs) {
    return super.post(url, body, this.getOptions(options));
  }

  public put(url: string, body: string, options?: RequestOptionsArgs) {
    return super.put(url, body, this.getOptions(options));
  }

  public delete(url: string, options?: RequestOptionsArgs) {
    return super.delete(url, this.getOptions(options));
  }

  protected getOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }
    if (this.__cookie.has('auth_token')) {
      options.headers.append('Authorization', `Bearer ${this.__cookie.get('auth_token')}`);
    }

    options.headers.append('Content-Type', 'application/json');

    return options;
  }
}
