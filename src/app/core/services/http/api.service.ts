import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

@Injectable()
export class ApiService {

  constructor(private _http: Http) {
  }

  public get(url: string) {
    return this._http
      .get(url)
      .map(this.extractData)
      .share();
  }

  public post(url: string, body: any) {
    return this._http
      .post(url, body)
      .map(this.extractData)
      .share();
  }

  public put(url: string, body: any) {
    return this._http
      .put(url, body)
      .map(this.extractData)
      .share();
  }

  public delete(url: string) {
    return this._http
      .delete(url)
      .map(this.extractData)
      .share();
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
}
