export const KEYS = {
  AUTH_TOKEN: 'auth_token',
  LOCATION: '__lctn'
};

export abstract class AbstractCookie {
  abstract get(key: String);

  abstract has(key: String)

  abstract getAll()
}
