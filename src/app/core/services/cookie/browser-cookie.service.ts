import {Injectable} from '@angular/core';
import {AbstractCookie} from "./AbstractCookie";
import {Cookie} from 'ng2-cookies/ng2-cookies';

@Injectable()
export class BrowserCookieService extends AbstractCookie {

  constructor() {
    super();
  }

  public getAll() {
    return Cookie.getAll();
  }

  public has(key) {
    return this.get(key) != undefined && this.get(key) != 'null';
  }

  public get(key) {
    return Cookie.get(key);
  }
}
