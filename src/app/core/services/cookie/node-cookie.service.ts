import {Injectable} from '@angular/core';
import {AbstractCookie} from "./AbstractCookie";

@Injectable()
export class NodeCookieService extends AbstractCookie {

  constructor() {
    super();
  }

  public getAll() {
    return Zone.current.get('req').cookies || {};
  }

  public has(key) {
    return this.get(key) != undefined && this.get(key) != 'null';
  }

  public get(key) {
    return this.getAll()[key];
  }
}
