import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhimBoxesComponent } from './whim-boxes/whim-boxes.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [WhimBoxesComponent],
  exports:[
    WhimBoxesComponent
  ]
})
export class SharedModule { }
