import { NgMobilePage } from './app.po';

describe('ng-mobile App', function() {
  let page: NgMobilePage;

  beforeEach(() => {
    page = new NgMobilePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
